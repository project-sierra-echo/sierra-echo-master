package echo.master.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("echo.master")
public class SierraEchoMaster {

  public static void main(String[] args) {
    SpringApplication.run(SierraEchoMaster.class);
  }
}
