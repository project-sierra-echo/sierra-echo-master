package echo.master.configuration.jpa;

import echo.common.domain.audit.jpa.JpaAuditingConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(JpaAuditingConfiguration.class)
@EntityScan({ "echo.master.domain.entity", "echo.common.domain.entity" })
public class JpaConfiguration {

}
